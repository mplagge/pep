#!/usr/bin/env python

size = 44

for i in range(size):
    line = list(size * ' ')
    line[i] = '*'
    line[size-i-1] = '*'
    print("".join(line))
