#!/usr/bin/env python

"""Script to translate morse code"""

import simpleaudio as sa
import morse
import tone_generator
import wav_convertor

def morse_to_string(morse_msg):

    msg = ''
    for morse_char in morse_msg:
        # print key or dictoinary acording to value 
        if morse_char == '': msg += ' '
        else:
            msg += list(morse.MORSE.keys())[list(morse.MORSE.values()).index(morse_char)]
    return msg

def binary_signal_msg_to_string(binary_signal):

    error = False
    morse = []
    char = ''
    i = 0
    while not error:
        while not error:
            if binary_signal[i] == 1 and binary_signal[i+1] == 0:
                char += '.'
                if binary_signal[i+2] == 0: i += 1
                else: i += 2
            elif binary_signal[i] == 1 and binary_signal[i+1] == 1:
                char += '-'
                try:
                    if binary_signal[i+4] == 0: i += 3
                    else: i += 4
                except:
                    print("Can't read characters propperly")
                    error = True
                    break
            elif binary_signal[i] == 0:
                i += 3
                break

        # remove double spaces
        if char != '':
            morse.append(char)
        elif len(morse) > 0:
            if morse[-1] != '':
                morse.append(char)
        elif len(morse) == 0:
            morse.append(char)

        char = ''
        if i >= len(binary_signal): break

    return morse
    
def play_tone():
    sa.WaveObject.from_wave_file('out.wav').play().wait_done()

def make_morse_audio(msg):
    msg_in_morse = [morse.MORSE[char] for char in msg]

    audio_buffer = []
    for char in msg_in_morse:
        for part in char:
            if part == '.':
                audio_buffer += tone_generator.make_sine(millis=50)
            elif part == '-':
                audio_buffer += tone_generator.make_sine(millis=150)
            elif part == ' ':
                audio_buffer += tone_generator.make_silence(millis=50*6)
            audio_buffer += tone_generator.make_silence(millis=50)
        audio_buffer += tone_generator.make_silence(millis=100)
    tone_generator.save_wav("out.wav", audio_buffer)

while True:
    msg = input('morse >>> ')

    if msg == ':l':
        binary_signal = wav_convertor.input_to_formatted_binary_signal()
        morse_msg = binary_signal_msg_to_string(binary_signal)
        print(morse_to_string(morse_msg))
    elif msg == ':q': break
    else:
        msg = msg.lower()
        if msg == '' or False in [x in morse.MORSE.keys() or x == ' ' for x in msg]: break
        print('Making audio...')
        make_morse_audio(msg)
        play_tone()
