#!/usr/bin/env python

"""Script to generate beeps and convert them to wav files"""

import math
import wave
import struct

SAMPLE_RATE = 44100.0

def make_silence(millis=50):
    """Genarate silence according to the sample rate and milisecconds and return it"""
    samples = millis * (SAMPLE_RATE / 1000.0)
    return [0.0 for x in range(int(samples))]

def make_sine(freq=880.0, millis=50):
    """Genrate sine according to the sample rate, freqency and milisecconds and return it"""

    samples = millis * (SAMPLE_RATE / 1000.0)
    buff = []
    for x in range(int(samples)):
        buff.append(math.sin(2 * math.pi * freq * (x / SAMPLE_RATE)))
    return buff

def save_wav(file_name, buff):
    """Save audio buffer as wav file"""

    audio_file = wave.open(file_name, 'w')

    nchannels = 1
    sampwidth = 2
    nframes = len(buff)
    comptype = 'NONE'
    compname = 'not compressed'
    audio_file.setparams((nchannels, sampwidth, SAMPLE_RATE, nframes, comptype, compname))

    # WAV files here are using short, 16 bit, signed integers for the
    # sample size.  So we multiply the floating point data we have by 32767, the
    # maximum value for a short integer.  NOTE: It is theortically possible to
    # use the floating point -1.0 to 1.0 data directly in a WAV file but not
    # obvious how to do that using the wave module in python.
    for sample in buff:
        audio_file.writeframes(struct.pack('h', int(sample * 32767.0)))

    audio_file.close()

def main():
    """Main"""
    buff = []
    buff += make_sine(millis=90)
    buff += make_silence(millis=200)
    buff += make_sine(millis=50)
    save_wav("out.wav", buff)

if __name__ == '__main__':
    main()
