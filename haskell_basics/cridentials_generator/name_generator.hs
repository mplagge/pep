import System.Random
import Data.Char

-- Script to generate random credentials

fistNames = ["Alex","Bob","Mike","Alis","Emma","Jenniffer"]
lastNames = ["Jacobs","Pender","Jackson","Torvalds"]
emailProviders = ["gmail","yahoo","protonmail","tutanota","tuta"]
topLevelDomains = ["com","nl","tk","de"]

replaceSpaceWithDot = map (\c -> if c==' ' then '.'; else c)

name :: IO String
name = do
    fistNameIndex <- randomRIO (0, length fistNames -1) :: IO Int
    lastNameIndex <- randomRIO (0, length lastNames -1) :: IO Int
    let fistName = fistNames !! fistNameIndex
    let lastName = lastNames !! lastNameIndex
    return $ fistName ++ " " ++ lastName

email :: IO String
email = do
    emailProvidersIndex <- randomRIO (0, length emailProviders -1) :: IO Int
    topLevelDomainsIndex <- randomRIO (0, length topLevelDomains -1) :: IO Int
    let emailProvider = emailProviders !! emailProvidersIndex
    let topLevelDomain = topLevelDomains !! topLevelDomainsIndex
    name' <- name
    let formated_name = replaceSpaceWithDot $ map toLower name'
    return $ formated_name ++ "@" ++ emailProvider ++ "." ++ topLevelDomain

age :: IO String  
age = do  
    c <- randomRIO (10,90) :: IO Int
    return $ show c

devider = replicate 35 '-'

main = do
    putStrLn "Creating random credientials"
    putStrLn devider
    putStr "Name  "; name >>= \l -> putStrLn l
    putStr "Email "; email >>= \l -> putStrLn l
    putStr "Age:  "; age >>= \l -> putStrLn l
    putStrLn devider
